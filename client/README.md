# [HTML5 Boilerplate](http://html5boilerplate.com)

HTML5 Boilerplate is a professional front-end template for building fast,
robust, and adaptable web apps or sites.

This project is the product of many years of iterative development and combined
community knowledge. It does not impose a specific development philosophy or
framework, so you're free to architect your code in the way that you want.

* Source: [https://github.com/h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate)
* Homepage: [http://html5boilerplate.com](http://html5boilerplate.com)
* Twitter: [@h5bp](http://twitter.com/h5bp)


## Quick start

Clone the git repo — `git clone
   https://bitbucket.org/rorisme/loopback-ember.git` - and checkout the tagged
   release you'd like to use.


## Features

* HTML5 ready. Use the new elements with confidence.
* Cross-browser compatible (Chrome, Opera, Safari, Firefox 3.6+, IE6+).
* Designed with progressive enhancement in mind.
* Includes [Normalize.css](http://necolas.github.com/normalize.css/) for CSS
  normalizations and common bug fixes.
* The latest [jQuery](http://jquery.com/) via CDN, with a local fallback.
* The latest [Modernizr](http://modernizr.com/) build for feature detection.
* IE-specific classes for easier cross-browser control.
* Placeholder CSS Media Queries.
* Useful CSS helpers.
* Default print CSS, performance optimized.
* Protection against any stray `console.log` causing JavaScript errors in
  IE6/7.
* An optimized Google Analytics snippet.
* Apache server caching, compression, and other configuration defaults for
  Grade-A performance.
* Cross-domain Ajax and Flash.
* "Delete-key friendly." Easy to strip out parts you don't need.
* Extensive inline and accompanying documentation.


## Documentation

Take a look at the [documentation table of contents](doc/TOC.md). This
documentation is bundled with the project, which makes it readily available for
offline reading and provides a useful starting point for any documentation you
want to write about your project.


# Ember.js

A starter kit for Ember, a framework for creating ambitious web applications.
Your Ember.js project is almost ready! Here's how to get started:

- Start writing your app in `js/app.js`.
- Describe your application HTML in `index.html`.
- During development, you can link to `js/libs/ember-*.js` to get the unminified version of Ember.js.
- Add CSS to `css/style.css`.
- Open `index.html` or navigate to the project's URL in your browser.


## Ember Data

Ember Data is a library for robustly managing model data in your Ember.js applications.

Ember Data is designed to be agnostic to the underlying persistence mechanism, so it works just as well with JSON APIs over HTTP as it does with streaming WebSockets or local IndexedDB storage.

It provides many of the facilities you'd find in server-side ORMs like ActiveRecord, but is designed specifically for the unique environment of JavaScript in the browser.

In particular, Ember Data uses Promises/A+-compatible promises from the ground up to manage loading and saving records, so integrating with other JavaScript APIs is easy.


## Tests

This starter kit comes with an integration test sample, written for QUnit runner.
You can run the tests by opening the `index.html?test` page in your browser.
The test is located in the `tests/tests.js` file. You can see how such an
integration test should be written, using QUnit assertions and ember-testing helpers.

For more information about Ember.js see [emberjs.com](http://www.emberjs.com)
For more information about Ember Data see the project's [documentation](https://github.com/emberjs/data)
For more information about ember-testing package see [ember-testing](http://emberjs.com/guides/testing/integration/).
For more information about the QUnit testing framework, see [QUnit](http://qunitjs.com/).
